import { FormControl } from '@angular/forms';

export class LoginValidator {
  static loginEmail(control: FormControl): any {
    function validateEmail(email) {
      var emailPattern = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
      if (emailPattern.test(String(email).toLowerCase())) {
        return null;
      }
      return {
        errorObj: 'Email Id is not valid'
      };
    }

    if (control.value == '') {
      return {
        errorObj: 'Email Id is required'
      };
    }

    if (control.value != '') {
      return validateEmail(control.value);
    }

    return null;
  }

  static loginPassword(control: FormControl): any {
    if (control.value == '') {
      return {
        errorObj: 'Password Id is required'
      };
    }

    if (control.value.length < 5) {
      return {
        errorObj: 'Minimum length should be 5'
      };
    }

    return null;
  }

  static nameFn(control: FormControl): any {
    if (control.value == '') {
      return {
        errorObj: 'Name is required'
      };
    }

    return null;
  }

  static mobileNumber(control: FormControl): any {
    function validateMobileNumber(value) {
      var mobileNumberPattern = /^[987][0-9]{9}$/;
      if (mobileNumberPattern.test(String(value).toLowerCase())) {
        return null;
      }
      return {
        errorObj: 'Mobile number is not valid'
      };
    }

    if (control.value == '') {
      return {
        errorObj: 'Mobile number is required'
      };
    }

    if (control.value.length < 9) {
      return {
        errorObj: 'Mobile number must be in 10 digit'
      };
    }

    if (control.value != '') {
      return validateMobileNumber(control.value);
    }

    return null;
  }

  static otpFn(control: FormControl): any {
    if (control.value == '') {
      return {
        errorObj: 'OTP is required'
      };
    }

    return null;
  }

  static groupName(control: FormControl): any {
    if (control.value == '') {
      return {
        errorObj: 'Group name is required'
      };
    }

    return null;
  }
}
