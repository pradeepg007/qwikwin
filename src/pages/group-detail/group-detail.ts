import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Api } from '../../provider/api/service';
import { PopoverController } from 'ionic-angular';
import { PopoverPage } from '../pop-over/pop-over';

@Component({
  selector: 'page-group-detail',
  templateUrl: 'group-detail.html'
})
export class GroupDetailPage {
  groupDetails: any = [];
  // tempData: any = {};
  constructor(public navCtrl: NavController, public api: Api, public popoverCtrl: PopoverController) { }

  ngOnInit() {
    this.api.getData('group-detail').subscribe(data => {
      let tempData: any = {};
      tempData = data;
      this.groupDetails = tempData.groupDetail;
      console.log('groupDetail', this.groupDetails);
    });
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({
      ev: myEvent
    });
  }
}
