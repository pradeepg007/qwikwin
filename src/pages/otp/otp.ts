import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { LoginValidator } from '../../validators/loginValidator';
import { Api } from '../../provider/api/service';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'otp-signUp',
  templateUrl: 'otp.html'
})
export class OTPPage {
  otpForm: FormGroup;
  finalErrorMessage: boolean = false;
  mobileNumberData: string = '';
  signUpPasswordData: string = '';
  localStorageObj: any = {};
  serverErrorMessage: any = '';

  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public api: Api,
    public formBuilder: FormBuilder,
    public navParams: NavParams
  ) {
    this.otpForm = formBuilder.group({
      otp: ['', LoginValidator.otpFn]
    });

    let tempData = this.navParams.get('mobileNoParam');
    let firstDigit = tempData.substr(0, 2);
    let lastDigit = tempData.substring(tempData.length - 2);
    this.mobileNumberData = `${firstDigit}xxxxxx${lastDigit}`;
    this.signUpPasswordData = this.navParams.get('signupPassParam');
  }

  save() {
    if (this.otpForm.value.otp.length) {
      this.finalErrorMessage = false;
      let objToPass = {
        mobileNumber: this.navParams.get('mobileNoParam'),
        otp: this.otpForm.value.otp
      };
      console.log('OTP objToPass', objToPass);
      this.api.post('verifyMobileNumberUsingOtp.ashx', objToPass).subscribe(data => {
        let tempData: any = data;
        if (tempData.statusCode == '200' && tempData.success) {
          this.navCtrl.setRoot(TabsPage);
          this.storeLoginDataToLocal();
        } else {
          this.serverErrorMessage = tempData.errorMessage;
          setTimeout(() => {
            this.serverErrorMessage = '';
          }, 2000);
        }
      });
    } else {
      this.finalErrorMessage = true;
    }
  }

  storeLoginDataToLocal = () => {
    this.localStorageObj.loginMobileNumber = this.mobileNumberData;
    this.localStorageObj.loginPass = this.signUpPasswordData;
    window.localStorage.setItem('qwikwinLogin', JSON.stringify(this.localStorageObj));
  };
}
