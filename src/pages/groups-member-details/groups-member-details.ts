import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { PopoverPage } from '../pop-over/pop-over';

/**
 * Generated class for the GroupsMemberDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-groups-member-details',
  templateUrl: 'groups-member-details.html',
})
export class GroupsMemberDetailsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl:PopoverController) {
  }
  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({
      ev: myEvent
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GroupsMemberDetailsPage');
  }

}
