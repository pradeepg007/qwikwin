import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { GroupPage } from '../group/group';

@Component({
  selector: 'menu-page',
  templateUrl: 'menu.html'
})
export class MenuPage {
  navArray: any =
    [

      {
        "pageName": "HomePage",
        "displayPageName": "Rules"
      },
      {
        "pageName": "GroupPage",
        "displayPageName": "Profile"
      },
      {
        "pageName": "leagues",
        "displayPageName": "Leagues"
      },
      {
        "pageName": "groups",
        "displayPageName": "Groups"
      },
      {
        "pageName": "leader-boards",
        "displayPageName": "Leader boards"
      },
      {
        "pageName": "settings",
        "displayPageName": "Settings"
      },
      {
        "pageName": "privacy-policy",
        "displayPageName": "Privacy Policy"
      },
      {
        "pageName": "about",
        "displayPageName": "About"
      }

    ]

  constructor(public navCtrl: NavController) {
    console.log(this.navArray[0]);
  }

  fnGotoPage(index) {
    //console.log(this.navArray[index].pageName)
    let moveToPage: string = this.navArray[index].pageName;
    console.log(moveToPage);
    // this.navCtrl.push(moveToPage);
  }


}
