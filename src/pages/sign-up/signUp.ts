import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { LoginValidator } from '../../validators/loginValidator';
import { Api } from '../../provider/api/service';
import { OTPPage } from '../otp/otp';

@Component({
  selector: 'page-signUp',
  templateUrl: 'signUp.html'
})
export class SignUpPage {
  signUpForm: FormGroup;
  finalErrorMessage: boolean = false;
  serverErrorMessage: any = '';

  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public api: Api,
    public formBuilder: FormBuilder
  ) {
    this.signUpForm = formBuilder.group({
      name: ['', LoginValidator.nameFn],
      emailId: ['', LoginValidator.loginEmail],
      mobileNumber: ['', LoginValidator.mobileNumber],
      signupPass: ['', LoginValidator.loginPassword]
    });
  }

  save() {
    if (
      this.signUpForm.value.name.length &&
      this.signUpForm.value.emailId.length &&
      this.signUpForm.value.mobileNumber.length &&
      this.signUpForm.value.signupPass.length
    ) {
      this.finalErrorMessage = false;

      let objToPass = {
        userName: this.signUpForm.value.name,
        emailId: this.signUpForm.value.emailId,
        mobileNumber: this.signUpForm.value.mobileNumber,
        password: this.signUpForm.value.signupPass
      };
      this.api.post('submitRegistrationDetails.ashx', objToPass).subscribe(data => {
        let tempData: any = data;
        if (tempData.statusCode == '200' && tempData.success) {
          this.navCtrl.push(OTPPage, {
            mobileNoParam: this.signUpForm.value.mobileNumber,
            signupPassParam: this.signUpForm.value.signupPass
          });
        } else {
          this.serverErrorMessage = tempData.errorMessage;
          setTimeout(() => {
            this.serverErrorMessage = '';
          }, 2000);
        }
      });
    } else {
      this.finalErrorMessage = true;
    }
  }
}
