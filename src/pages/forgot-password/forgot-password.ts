import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { LoginValidator } from  '../../validators/loginValidator';
import { Api } from '../../provider/api/service';
import {OTPPage} from '../otp/otp';

@Component({
  selector: 'forgot-password',
  templateUrl: 'forgot-password.html'
})
export class ForgotPasswordPage {

   forgotPassForm: FormGroup;
   finalErrorMessage:boolean = false;
   
   

  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public api: Api, public formBuilder: FormBuilder, public navParams: NavParams) {
     this.forgotPassForm = formBuilder.group({
        forgotPassMobile: ['',LoginValidator.mobileNumber]
    });
  }

    save(){
            if(this.forgotPassForm.value.forgotPassMobile.length){
              this.navCtrl.push(OTPPage,{
                mobileNoParam : this.forgotPassForm.value.forgotPassMobile,
                signupPassParam: ''
              });
              this.finalErrorMessage = false;
            } else{
                  this.finalErrorMessage = true;
            }
          }

    //       storeLoginDataToLocal = () => {
    //         this.localStorageObj.loginMobileNumber = this.mobileNumberData;
    //         this.localStorageObj.loginPass = this.signUpPasswordData;
    //          window.localStorage.setItem('qwikwinLogin', JSON.stringify(this.localStorageObj))
    // }
}
