import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { FootballPage } from '../football/football';
import { Api } from '../../provider/api/service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  gamesList: any = [];
  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public api: Api) {}

  ionViewDidLoad() {
    this.api.getData('getLeagueList').subscribe(data => {
      let tempData: any = data;
      let tempSuccessData =
        typeof tempData.successMessage == 'string' ? JSON.parse(tempData.successMessage) : tempData.successMessage;
      this.gamesList = tempSuccessData;
    });
  }

  goToPage = () => {
    this.navCtrl.push(FootballPage);
  };
}
