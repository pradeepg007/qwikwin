import { Component, ViewChild, OnInit } from '@angular/core';
import { NavController, Slides, LoadingController } from 'ionic-angular';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import _ from 'lodash';
import { Api } from '../../../provider/api/service';

@Component({
  selector: 'match-predictions',
  templateUrl: 'match-predictions.html'
})
export class MatchPredictionPage {
  @ViewChild(Slides) slides: Slides;
  @ViewChild('matchFixtureSlider') matchFixtureSlider: Slides;

  tempAllData: any = {};
  tournamentId: number = 0;
  matchDateArray: any = [];
  monthNamesAray: any = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  matchsOnSelectedDate: any = [];
  selectedMatchDateId: number = 0;

  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public api: Api) {}

  ngOnInit() {
    this.api.getData('getTournamentsSoccer').subscribe(data => {
      let tempData: any = data;
      let tempSuccessData =
        typeof tempData.successMessage == 'string' ? JSON.parse(tempData.successMessage) : tempData.successMessage;
      this.tempAllData = tempSuccessData;
      let tempMatchLength = tempSuccessData.length;
      for (let i = 0; i < tempMatchLength; i++) {
        let matchesArray = tempSuccessData[i].matches;
        let matchesArrayLength = matchesArray.length;

        for (let j = 0; j < matchesArrayLength; j++) {
          let dayTemp = new Date(matchesArray[j].matchDate).getDate();
          let monthTemp = new Date(matchesArray[j].matchDate).getMonth();
          let DDMM = `${dayTemp} ${this.monthNamesAray[monthTemp]}`;
          matchesArray[j].displayData = DDMM;
          let filteredObj = _.find(this.matchDateArray, obj => {
            return obj.displayData === matchesArray[j].displayData;
          });
          if (!filteredObj) {
            this.matchDateArray.push(matchesArray[j]);
          }
        }
      }

      let todayDate = new Date().getDate();
      let todayMonth = new Date().getMonth();
      let todayDDMM = `${todayDate} ${this.monthNamesAray[todayMonth]}`;
      let filterTodayObj = _.find(this.matchDateArray, obj => {
        return obj.displayData === todayDDMM;
      });
      if (filterTodayObj) {
        this.filterMatch(filterTodayObj, 1);
      }
    });
  }

  filterMatch = (data, index) => {
    this.selectedMatchDateId = index;
    this.matchsOnSelectedDate = [];
    let tempDateByType: any;

    _.forEach(this.tempAllData[0].matches, obj => {
      if (obj.displayData == data.displayData) {
        this.matchsOnSelectedDate.push(obj);
      }
    });

    _.forEach(this.matchsOnSelectedDate, obj => {
      let filteredObjTeamA = _.find(this.tempAllData[0].teams, team => {
        return team.id == obj.teamA_id;
      });
      let filteredObjTeamB = _.find(this.tempAllData[0].teams, objInner => {
        return objInner.id == obj.teamB_id;
      });
      obj.teamADisplayInfo = filteredObjTeamA;
      obj.teamBDisplayInfo = filteredObjTeamB;
      obj.buttonDisableFlag = new Date(obj.matchDate) < new Date();
      obj.playerList = filteredObjTeamA.playerList.split(',').concat(filteredObjTeamB.playerList.split(','));
    });
    setTimeout(() => {
      if (this.matchFixtureSlider && this.matchFixtureSlider.length()) {
        this.matchFixtureSlider.slideTo(0, 500);
      }
    }, 600);
    console.log('filteredObjTeamA', this.matchsOnSelectedDate);
  };

  increaseGoal = (cardInfo: any, teamType: any) => {
    console.log('cardInfo', cardInfo);
    console.log('teamType', teamType);
    // cardInfo.predictor[teamType] = cardInfo.predictor[teamType] + 1;
    // this.decideWinner(cardInfo);
  };

  decreaseGoal = (cardInfo: any, teamType: any) => {
    console.log('cardInfo', cardInfo);
    console.log('teamType', teamType);
    // if (cardInfo.predictor[teamType] > 0) {
    //   // cardInfo.predictor[teamType] = cardInfo.predictor[teamType] - 1;
    //   // this.decideWinner(cardInfo);
    // }
  };

  decideWinner = info => {
    if (info.predictor.awayTeamGoalPredection > info.predictor.homeTeamGoalPredection) {
      info.predictor.winner = info['awayTeamDisplay'].name;
    } else if (info.predictor.awayTeamGoalPredection < info.predictor.homeTeamGoalPredection) {
      info.predictor.winner = info['homeTeamDisplay'].name;
    } else {
      info.predictor.winner = 'Draw';
    }
  };

  slideNext = (): void => {
    this.slides.slideNext();
  };

  slidePrev = (): void => {
    this.slides.slidePrev();
  };

  selectMatchTopScorrer = (event: { component: SelectSearchableComponent; value: any }) => {
    console.log('terminal:', event.value);
  };

  submitMatchPredection = () => {
    console.log('data', this.tempAllData);
  };

  // ngOnInit() {
  //   this.api.getData('getTournamentsSoccer').subscribe(data => {
  //     // console.log("data",data);
  //     let tempData: any = data;
  //     let tempSuccessMessage = JSON.parse(tempData.successMessage);
  //     this.tempAllData = tempSuccessMessage;
  //     let groups = this.tempAllData.groups;
  //     let knockouts = this.tempAllData.knockout;
  //     for (let group in groups) {
  //       let matchesArray = groups[group].matches;
  //       let tempMatchLength = matchesArray.length;
  //       for (let i = 0; i < tempMatchLength; i++) {
  //         let dayTemp = new Date(matchesArray[i].date).getDate();
  //         let monthTemp = new Date(matchesArray[i].date).getMonth();
  //         let DDMM = `${dayTemp} ${this.monthNamesAray[monthTemp]}`;
  //         matchesArray[i].displayData = DDMM;
  //         let filteredObj = _.find(this.matchDateArray, obj => {
  //           return obj.displayData === matchesArray[i].displayData;
  //         });
  //         if (!filteredObj) {
  //           this.matchDateArray.push(matchesArray[i]);
  //         }
  //       }
  //     }
  //     this.matchDateArray.sort(function compare(a, b) {
  //       let dateA: any = new Date(a.displayData);
  //       let dateB: any = new Date(b.displayData);
  //       return dateA - dateB;
  //     });
  //     for (let knockout in knockouts) {
  //       let matchesArray = knockouts[knockout].matches;
  //       let tempMatchLength = matchesArray.length;
  //       for (let i = 0; i < tempMatchLength; i++) {
  //         let dayTemp = new Date(matchesArray[i].date).getDate();
  //         let monthTemp = new Date(matchesArray[i].date).getMonth();
  //         let DDMM = `${dayTemp} ${this.monthNamesAray[monthTemp]}`;
  //         matchesArray[i].displayData = DDMM;
  //         let filteredObj = _.find(this.matchDateArray, obj => {
  //           return obj.displayData === matchesArray[i].displayData;
  //         });
  //         if (!filteredObj) {
  //           this.matchDateArray.push(matchesArray[i]);
  //         }
  //       }
  //     }

  //     let todayDay = new Date().getDate();
  //     let todayMonth = new Date().getMonth();
  //     let compareDate = `${todayDay} ${this.monthNamesAray[todayMonth]}`;

  //     let dataToPass = _.find(this.matchDateArray, obj => {
  //       return obj.displayData == compareDate;
  //     });
  //     if (dataToPass) {
  //       this.filterMatch(dataToPass, 0);
  //     } else {
  //       this.filterMatch(this.matchDateArray[0], 0);
  //     }
  //     // console.log('this.matchDateArray', this.matchDateArray);
  //   });
  // }
}
