import { Component } from '@angular/core';

import { MatchPredictionPage } from './match-predictions/match-predictions';
import { TournamentPredictionPage } from './tournament-predictions/tournament-predictions';

@Component({
  selector: 'football-home',
  templateUrl: 'football.html'
})
export class FootballPage {
  componentFlag: string = 'matchPredection';
  constructor() {}
  changePredectionComponent = data => {
    console.log('Data', data);
    this.componentFlag = data;
  };
}
