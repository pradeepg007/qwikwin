import { Component, ViewChild } from '@angular/core';
import { NavController, Slides, LoadingController } from 'ionic-angular';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { Api } from '../../../provider/api/service';

@Component({
  selector: 'tournament-predictions',
  templateUrl: 'tournament-predictions.html'
})
export class TournamentPredictionPage {
  tempAllData: any = {};
  fifaCounterys: any = [];
  winnerCounterySelected: any = { flag: 'https://www.icon2s.com/wp-content/uploads/2013/07/ios7-flag-icon.png' };
  goldenBallWinner: any = [];
  goldenBallWinnerSelected: any = '';
  goldenBootWinner: any = [];
  goldenBootWinnerSelected: any = '';
  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public api: Api) {
    this.winnerCounterySelected = { flag: 'https://www.icon2s.com/wp-content/uploads/2013/07/ios7-flag-icon.png' };
    console.log(this.winnerCounterySelected, 'winnerCounterySelected');
  }

  ngOnInit() {
    this.api.getData('getWorldCup2018').subscribe(data => {
      // console.log("data", data);
      let tempData: any = data;
      let tempSuccessData = JSON.parse(tempData.successMessage);
      this.tempAllData = tempSuccessData;
      this.fifaCounterys = this.tempAllData.teams;
      this.goldenBallWinner = this.tempAllData.teamPlaners['1'];
      this.goldenBootWinner = this.tempAllData.teamPlaners['1'];
    });
  }

  counteryChange(event: { component: SelectSearchableComponent; value: any }) {
    this.winnerCounterySelected = event.value;
  }

  goldenBallChange(event: { component: SelectSearchableComponent; value: any }) {
    this.goldenBallWinnerSelected = event.value;
  }

  goldenBootChange(event: { component: SelectSearchableComponent; value: any }) {
    this.goldenBootWinnerSelected = event.value;
  }

  submitTournamentPredection = () => {
    console.log('tournament-predictions', this.tempAllData);
  };

  ionViewDidLoad() {
    console.log('tournament-predictions');
  }
}
