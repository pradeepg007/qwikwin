import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { LoginValidator } from '../../validators/loginValidator';
import { Api } from '../../provider/api/service';

import { TabsPage } from '../tabs/tabs';
import { SignUpPage } from '../sign-up/signUp';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  loginForm: FormGroup;
  finalErrorMessage: boolean = false;
  serverErrorMessage: any = '';

  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public api: Api,
    public formBuilder: FormBuilder
  ) {
    this.loginForm = formBuilder.group({
      loginMobileNumber: ['', LoginValidator.mobileNumber],
      loginPass: ['', LoginValidator.loginPassword]
    });
  }

  save = () => {
    if (this.loginForm.value.loginMobileNumber.length && this.loginForm.value.loginPass.length) {
      this.finalErrorMessage = false;
      let objToPass = {
        mobileNumber: this.loginForm.value.loginMobileNumber,
        password: this.loginForm.value.loginPass
      };
      this.api.post('login.ashx', objToPass).subscribe(data => {
        let tempData: any = data;
        if (tempData.statusCode == '200' && tempData.success) {
          this.storeLoginDataToLocal();
          this.navCtrl.push(TabsPage);
        } else {
          this.serverErrorMessage = tempData.errorMessage;
          setTimeout(() => {
            this.serverErrorMessage = '';
          }, 2000);
        }
      });
    } else {
      this.finalErrorMessage = true;
    }
  };

  storeLoginDataToLocal = () => {
    window.localStorage.setItem('qwikwinLogin', JSON.stringify(this.loginForm.value));
  };

  goToSignUpPage = () => {
    this.navCtrl.push(SignUpPage);
  };

  goToForgotPassPage = () => {
    this.navCtrl.push(ForgotPasswordPage);
  };
}
