import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { LoginValidator } from '../../validators/loginValidator';
import { Api } from '../../provider/api/service';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'create-group',
  templateUrl: 'create-group.html'
})
export class CreateGroupPage {
  createGroupForm: FormGroup;
  finalErrorMessage: boolean = false;

  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public api: Api,
    public formBuilder: FormBuilder
  ) {
    this.createGroupForm = formBuilder.group({
      groupName: ['', LoginValidator.groupName]
    });
  }

  save() {
    if (this.createGroupForm.value.groupName.length) {
      this.finalErrorMessage = false;
    } else {
      this.finalErrorMessage = true;
    }
  }
}
