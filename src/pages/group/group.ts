import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { PopoverPage } from '../pop-over/pop-over';
import { Api } from '../../provider/api/service';
import { CreateGroupPage } from '../create-group/create-group';

@Component({
  selector: 'page-group',
  templateUrl: 'group.html'
})
export class GroupPage {
  groupList: any = [];
  constructor(public navCtrl: NavController, public api: Api, public popoverCtrl: PopoverController) { }

  ngOnInit() {
    this.api.getData('group-list').subscribe(data => {
      this.groupList = data;
      console.log(this.groupList);
    });
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({
      ev: myEvent
    });
  }

  goToNextPage() {
    this.navCtrl.push(CreateGroupPage);
  }
}
