import { Component } from '@angular/core';

import { GroupPage } from '../group/group';
import { HomePage } from '../home/home';
import { FootballPage } from '../football/football';
import { MenuPage } from '../menu/menu';

// import { PopOverPage } from '../pop-over/pop-over';
import { GroupDetailPage } from '../group-detail/group-detail';
import { GroupInvitePage } from '../group-invite/group-invite';
import { GroupsMemberDetailsPage } from '../groups-member-details/groups-member-details';

@Component({
  selector: 'bottom-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root = HomePage;

  tab2Root = GroupPage;
  tab3Root = MenuPage;

  constructor() { }

  moreOptions() {
    console.log('moreOptions');
  }
}
