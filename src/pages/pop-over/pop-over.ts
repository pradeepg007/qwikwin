import { Component } from '@angular/core';
import { ViewController, NavParams, NavController } from 'ionic-angular';
import { CreateGroupPage } from '../create-group/create-group';
import { GroupDetailPage } from '../group-detail/group-detail';

/**
 * Generated class for the PopOverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  // selector: 'page-pop-over',
  template: `<ion-list>

  <button ion-item (click)="fnCreateGroup()">Ctreate Group</button>
  <button ion-item (click)="joinExisting()">Join Existing</button>

  </ion-list>`
})

export class PopoverPage {
  constructor(public viewCtrl: ViewController, public navParams: NavParams, public navCtrl: NavController) {
    console.log(this.navParams.data);
  }

  clofnCreateGroupse() {
    this.navCtrl.push(CreateGroupPage);
    this.viewCtrl.dismiss();
  }

  joinExisting() {
    this.navCtrl.push(GroupDetailPage);
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopOverPage');
  }

}
