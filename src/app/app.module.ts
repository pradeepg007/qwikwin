import { NgModule, ErrorHandler } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { SelectSearchableModule } from 'ionic-select-searchable';

import { Api } from '../provider/api/service';
import { MyApp } from './app.component';

import { GroupPage } from '../pages/group/group';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { SignUpPage } from '../pages/sign-up/signUp';
import { OTPPage } from '../pages/otp/otp';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { CreateGroupPage } from '../pages/create-group/create-group';
import { FootballPage } from '../pages/football/football';
import { MatchPredictionPage } from '../pages/football/match-predictions/match-predictions';
import { TournamentPredictionPage } from '../pages/football/tournament-predictions/tournament-predictions';
import { MenuPage } from '../pages/menu/menu';
import { GroupDetailPage } from '../pages/group-detail/group-detail';
import { GroupInvitePage } from '../pages/group-invite/group-invite';
import { GroupsMemberDetailsPage } from '../pages/groups-member-details/groups-member-details';
import { PopoverPage } from '../pages/pop-over/pop-over';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    GroupPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    SignUpPage,
    OTPPage,
    ForgotPasswordPage,
    CreateGroupPage,
    FootballPage,
    MatchPredictionPage,
    TournamentPredictionPage,
    MenuPage,
    GroupDetailPage,
    GroupInvitePage,
    GroupsMemberDetailsPage,
    PopoverPage
    
  ],
  imports: [BrowserModule, HttpClientModule, SelectSearchableModule, IonicModule.forRoot(MyApp)],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    GroupPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    SignUpPage,
    OTPPage,
    ForgotPasswordPage,
    CreateGroupPage,
    FootballPage,
    MatchPredictionPage,
    TournamentPredictionPage,
    MenuPage,
    GroupDetailPage,
    GroupInvitePage,
    GroupsMemberDetailsPage,
    PopoverPage

  ],
  providers: [Api, StatusBar, SplashScreen, { provide: ErrorHandler, useClass: IonicErrorHandler }]
})
export class AppModule {}
